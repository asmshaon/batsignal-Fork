Modification Note
=================
Batsignal-Fork is forked from [electrickite/batsignal](https://github.com/electrickite/batsignal).
Originally it was a battery monitoring and notification daemon.

Hardcoded battery level
-----------------------
New dafault battery levels (and effects)

	* Full: 80 (Notification)
	* Warning: 41 (Notification)
	* Critical: 35 (Hibernate)
	* Danger: 31 (Shutdown)

Executed Bash Script
--------------------
Everytime Batsignal creates a notification( when battery reaches or goes below one of the designated level), this will execute
an external bash script.

Current Script details:

	* Symlink file: /opt/low_battery_script_link
	* Script file: ~/scripts/power-control/low_battery_processing_script.sh

To Make the Symlink:

```bash
[ -f ~/scripts/power-control/low_battery_processing_script.sh ] && sudo ln -s ~/scripts/power-control/low_battery_processing_script.sh /opt/low_battery_script_link
```

This script is in my dotfile directory already. So no need to keep it here. Especially because it will go out-of-date very fast.
Actual script file can be changed; but symlink file is defined in batsignal.c as "scriptPath" variable.


Linux Mint Install
------------------

### Dependency

```bash
sudo apt install gcc make pkg-config libnotify-dev -y
```

### Batsignal
```bash
make
sudo make install
```
### Systemd

```bash
sudo make install-service
```

And can be enabled and started with:

```bash
sudo systemctl daemon-reload
systemctl --user enable batsignal.service
systemctl --user start batsignal.service
```

To customize options for services:

```bash
cd batsignal-Fork
sed -i "s/ExecStart=batsignal/ExecStart=batsignal -d 30 -c 48 -w 57 -f 90/" ./batsignal.service
sudo make install-service
sudo systemctl daemon-reload
```
Run Batsignal
-------------
* with dynamic config

```bash
batsignal -d 30 -c 48 -w 57 -f 90
```

* As background daemon

```bash
batsignal -b
```

[Origianl README](./README_old.md)
